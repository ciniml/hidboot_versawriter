#ifndef	__USBCONFIG_H__
#define	__USBCONFIG_H__

#if defined (__AVR_ATmega644__) || defined (__AVR_ATmega644P__)
#include "usbconfig_mega644.h"
#elif defined (__AVR_ATmega88__)
#include "usbconfig_mega88.h"
#elif defined (__AVR_ATmega328__) || defined(__AVR_ATmega328P__)
#include "usbconfig_mega328.h"
#else
#error This AVR is not supported.
#endif

#endif	//__USBCONFIG_H__
